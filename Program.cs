﻿using DemoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DemoEF
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Musician toUpdate = new Musician() { Id = 1, FavoriteGenre = "Folk" };
            //UpdateMusician(toUpdate);

            DeleteMusician(3);
            AddMusician(new Musician() { Name = "Flea", FavoriteGenre = "Rock", BandId = 1, InstrumentId = 1 });



            List<Musician> musicians = GetMusicians();

            foreach (Musician m in musicians)
                Console.WriteLine($"{m.Id} {m.Name} {m.FavoriteGenre}");
        }

        // get data
        public static List<Musician> GetMusicians()
        {
            List<Musician> musicians = new List<Musician>();

            BattleOfTheBandsDbContext db = new BattleOfTheBandsDbContext();

            musicians = db.Musicians.ToList();

            return musicians;
        }

        // add data
        public static void AddMusician(Musician musician)
        {
            BattleOfTheBandsDbContext dbContext = new BattleOfTheBandsDbContext();

            dbContext.Musicians.Add(musician);

            dbContext.SaveChanges();
        }

        // update data
        public static void UpdateMusician(Musician musician)
        {
            BattleOfTheBandsDbContext dbContext = new BattleOfTheBandsDbContext();

            Musician musicianToUpdate = dbContext.Musicians.Find(musician.Id);

            musicianToUpdate.FavoriteGenre = musician.FavoriteGenre;

            dbContext.SaveChanges();
        }

        // delete data
        public static void DeleteMusician(int id)
        {
            BattleOfTheBandsDbContext db = new BattleOfTheBandsDbContext();
            Musician musician = db.Musicians.Find(id);

            db.Musicians.Remove(musician);
            db.SaveChanges();
        }
    }
}
