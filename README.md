# Class Demo with Entity Framework

## Description
A project showcasing the use of Microsoft's ORM - Entity Framework.
Using a "BattleOfTheBands" database, with Musican, Instrument and Band models.

## Contributors
Warren West
