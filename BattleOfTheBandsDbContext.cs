﻿using DemoEF.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoEF
{
    public class BattleOfTheBandsDbContext : DbContext
    {
        public DbSet<Musician> Musicians { get; set; }
        public DbSet<Band> Bands { get; set; }
        public DbSet<Instrument> Instruments { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<GenreBand> GenreBands { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(GetConnectionString());
        }

        private string GetConnectionString()
        {
            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder();
            sb.DataSource = "localhost\\SQLEXPRESS";
            sb.InitialCatalog = "BattleOfTheBands";
            sb.IntegratedSecurity = true;

            return sb.ConnectionString;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed Instrument Data
            modelBuilder.Entity<Instrument>().HasData(SeedDataHelper.GetInstruments());

            // Seed Band Data
            modelBuilder.Entity<Band>().HasData(SeedDataHelper.GetBands());

            // Seed Musician Data
            modelBuilder.Entity<Musician>().HasData(SeedDataHelper.GetMusicians());

            // Set up composite key
            modelBuilder.Entity<GenreBand>().HasKey(genreBand => new { genreBand.BandId, genreBand.GenreId });
        }
    }
}
