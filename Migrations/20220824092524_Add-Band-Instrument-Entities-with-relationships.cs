﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DemoEF.Migrations
{
    public partial class AddBandInstrumentEntitieswithrelationships : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BandId",
                table: "Musicians",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InstrumentId",
                table: "Musicians",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Bands",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bands", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Instruments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Instruments", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Musicians_BandId",
                table: "Musicians",
                column: "BandId");

            migrationBuilder.CreateIndex(
                name: "IX_Musicians_InstrumentId",
                table: "Musicians",
                column: "InstrumentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Musicians_Bands_BandId",
                table: "Musicians",
                column: "BandId",
                principalTable: "Bands",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Musicians_Instruments_InstrumentId",
                table: "Musicians",
                column: "InstrumentId",
                principalTable: "Instruments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Musicians_Bands_BandId",
                table: "Musicians");

            migrationBuilder.DropForeignKey(
                name: "FK_Musicians_Instruments_InstrumentId",
                table: "Musicians");

            migrationBuilder.DropTable(
                name: "Bands");

            migrationBuilder.DropTable(
                name: "Instruments");

            migrationBuilder.DropIndex(
                name: "IX_Musicians_BandId",
                table: "Musicians");

            migrationBuilder.DropIndex(
                name: "IX_Musicians_InstrumentId",
                table: "Musicians");

            migrationBuilder.DropColumn(
                name: "BandId",
                table: "Musicians");

            migrationBuilder.DropColumn(
                name: "InstrumentId",
                table: "Musicians");
        }
    }
}
