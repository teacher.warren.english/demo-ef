﻿// <auto-generated />
using System;
using DemoEF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DemoEF.Migrations
{
    [DbContext(typeof(BattleOfTheBandsDbContext))]
    [Migration("20220824092524_Add-Band-Instrument-Entities-with-relationships")]
    partial class AddBandInstrumentEntitieswithrelationships
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.17")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DemoEF.Models.Band", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Bands");
                });

            modelBuilder.Entity("DemoEF.Models.Instrument", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Instruments");
                });

            modelBuilder.Entity("DemoEF.Models.Musician", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("BandId")
                        .HasColumnType("int");

                    b.Property<string>("FavoriteGenre")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("InstrumentId")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("BandId");

                    b.HasIndex("InstrumentId");

                    b.ToTable("Musicians");
                });

            modelBuilder.Entity("DemoEF.Models.Musician", b =>
                {
                    b.HasOne("DemoEF.Models.Band", "Band")
                        .WithMany("Musicians")
                        .HasForeignKey("BandId");

                    b.HasOne("DemoEF.Models.Instrument", "Instrument")
                        .WithMany("Musicians")
                        .HasForeignKey("InstrumentId");

                    b.Navigation("Band");

                    b.Navigation("Instrument");
                });

            modelBuilder.Entity("DemoEF.Models.Band", b =>
                {
                    b.Navigation("Musicians");
                });

            modelBuilder.Entity("DemoEF.Models.Instrument", b =>
                {
                    b.Navigation("Musicians");
                });
#pragma warning restore 612, 618
        }
    }
}
