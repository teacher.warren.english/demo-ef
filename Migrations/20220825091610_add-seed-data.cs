﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DemoEF.Migrations
{
    public partial class addseeddata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Musicians_Bands_BandId",
                table: "Musicians");

            migrationBuilder.DropForeignKey(
                name: "FK_Musicians_Instruments_InstrumentId",
                table: "Musicians");

            migrationBuilder.AlterColumn<int>(
                name: "InstrumentId",
                table: "Musicians",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BandId",
                table: "Musicians",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "Bands",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "The Accelerators" });

            migrationBuilder.InsertData(
                table: "Instruments",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Guitar" },
                    { 2, "Drums" },
                    { 3, "Piano" }
                });

            migrationBuilder.InsertData(
                table: "Musicians",
                columns: new[] { "Id", "BandId", "FavoriteGenre", "InstrumentId", "Name" },
                values: new object[] { 1, 1, "Heavy Metal", 1, "Jimmy Hendrix" });

            migrationBuilder.InsertData(
                table: "Musicians",
                columns: new[] { "Id", "BandId", "FavoriteGenre", "InstrumentId", "Name" },
                values: new object[] { 2, 1, "Funk", 2, "Joey Jordison" });

            migrationBuilder.InsertData(
                table: "Musicians",
                columns: new[] { "Id", "BandId", "FavoriteGenre", "InstrumentId", "Name" },
                values: new object[] { 3, 1, "Pop", 3, "Elton John" });

            migrationBuilder.AddForeignKey(
                name: "FK_Musicians_Bands_BandId",
                table: "Musicians",
                column: "BandId",
                principalTable: "Bands",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Musicians_Instruments_InstrumentId",
                table: "Musicians",
                column: "InstrumentId",
                principalTable: "Instruments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Musicians_Bands_BandId",
                table: "Musicians");

            migrationBuilder.DropForeignKey(
                name: "FK_Musicians_Instruments_InstrumentId",
                table: "Musicians");

            migrationBuilder.DeleteData(
                table: "Musicians",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Musicians",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Musicians",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Bands",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Instruments",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Instruments",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Instruments",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.AlterColumn<int>(
                name: "InstrumentId",
                table: "Musicians",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "BandId",
                table: "Musicians",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Musicians_Bands_BandId",
                table: "Musicians",
                column: "BandId",
                principalTable: "Bands",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Musicians_Instruments_InstrumentId",
                table: "Musicians",
                column: "InstrumentId",
                principalTable: "Instruments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
