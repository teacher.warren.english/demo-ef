﻿using DemoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoEF
{
    public class SeedDataHelper
    {
        public static List<Instrument> GetInstruments()
        {
            List<Instrument> instruments = new List<Instrument>() {
                new Instrument(){ Id = 1, Name = "Guitar" },
                new Instrument(){ Id = 2, Name = "Drums" },
                new Instrument(){ Id = 3, Name = "Piano" },
            };

            return instruments;
        }
        
        public static List<Band> GetBands()
        {
            List<Band> bands = new List<Band>()
            {
                new Band(){ Id = 1, Name = "The Accelerators" },
            };

            return bands;
        }
         
        public static List<Musician> GetMusicians()
        {
            List<Musician> musicians = new List<Musician>()
            {
                new Musician(){ Id = 1, Name = "Jimmy Hendrix", FavoriteGenre = "Heavy Metal", BandId = 1, InstrumentId = 1},
                new Musician(){ Id = 2, Name = "Joey Jordison", FavoriteGenre = "Funk", BandId = 1, InstrumentId = 2},
                new Musician(){ Id = 3, Name = "Elton John", FavoriteGenre = "Pop", BandId = 1, InstrumentId = 3},
            };

            return musicians;
        }
    }
}
