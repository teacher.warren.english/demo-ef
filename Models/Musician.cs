﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoEF.Models
{
    public class Musician
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FavoriteGenre { get; set; } // nvarchar(max) -> big!

        // navigation properties
        public int InstrumentId { get; set; } // FK
        public Instrument Instrument { get; set; }
        public int BandId { get; set; } // FK
        public Band Band { get; set; }
    }
}
