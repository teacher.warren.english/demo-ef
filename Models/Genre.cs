﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoEF.Models
{
    public class Genre
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string Description { get; set; }

        // nav props
        public ICollection<Band> Bands { get; set; }
    }
}
