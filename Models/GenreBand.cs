﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoEF.Models
{
    public class GenreBand
    {
        // our properties
        public int Influence { get; set; }

        // foreign keys
        public int BandId { get; set; }
        public Band Band { get; set; }
        public int GenreId { get; set; }
        public Genre Genre { get; set; }
    }
}
