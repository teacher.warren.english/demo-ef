﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoEF.Models
{
    public class Band
    {
        public int Id { get; set; }
        public string Name { get; set; }

        // navigation props
        public ICollection<Musician> Musicians { get; set; }
        public ICollection<Genre> Genres { get; set; }
    }
}
