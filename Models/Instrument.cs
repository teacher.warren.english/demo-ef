﻿using System.Collections.Generic;

namespace DemoEF.Models
{
    public class Instrument
    {
        public int Id { get; set; }
        public string Name { get; set; }

        // nav props
        public ICollection<Musician> Musicians { get; set; }
    }
}